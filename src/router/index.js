import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Board from '@/components/Board'
import ContentDetail from '@/components/ContentDetail';
import Create from '@/components/Create';
import TodoList from '@/components/TodoList';
import TodoListWithSearch from '@/components/TodoListWithSearch';
import SimLogin from '@/components/sim/SimLogin';
import SimMain from '@/components/sim/SimMain';
import SimJoin from '@/components/sim/SimJoin';


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/board/free',
      name: 'Board',
      component: Board
    },
    {
      path: '/board/free/detail/:contentId',
      name: 'ContentDetail',
      component: ContentDetail
    },
    {
      // 뒤에 컨텐트아이디를 붙여줘야함 필수는아니고 선택적으로 들어가기에 뒤에 물음표붙여줌
      path: '/board/free/create/:contentId?',
      name: 'Create',
      component: Create
    },
    {
      path: '/todoList',
      name: 'TodoList',
      component: TodoList
    },
    {
      path: '/todoListWithSearch',
      name: 'TodoListWithSearch',
      component: TodoListWithSearch
    },
    {
      path: '/tabPractice',
      name: 'TabPractice',
      component: TodoListWithSearch
    },
    {
      path: '/sim/main',
      name: 'SimMain',
      component: SimMain
    },
    {
      path: '/simLogin',
      name: 'SimLogin',
      component: SimLogin
    },
    {
      path: '/simJoin',
      name: 'SimJoin',
      component: SimJoin
    }
  ]
})